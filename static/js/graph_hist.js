window.onload = function () {
    
    var chart1 = new CanvasJS.Chart("chartContainer1", {
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        title:{
            text: "Top Oil Reserves"
        },
        axisY: {
            title: "Reserves(MMbbl)"
        },
        data: [{
            type: "column",
            showInLegend: true,
            legendMarkerColor: "grey",
            legendText: "MMbbl = one million barrels",
            dataPoints: [
                { y: 300878, label: "Venezuela" },
                { y: 266455,  label: "Saudi" },
                { y: 169709,  label: "Canada" },
                { y: 158400,  label: "Iran" },
                { y: 142503,  label: "Iraq" },
                { y: 101500, label: "Kuwait" },
                { y: 97800,  label: "UAE" },
                { y: 80000,  label: "Russia" }
            ]
        }]
    });
    
    var chart2 = new CanvasJS.Chart("chartContainer2", {
        animationEnabled: true,
        theme: "light2", //"light1", "dark1", "dark2"
        title:{
            text: "Division of Products Sold in 2nd Quarter"
        },
        axisY:{
            interval: 10,
            suffix: "%"
        },
        toolTip:{
            shared: true
        },
        data:[
            {
                type: "stackedBar100",
                toolTipContent: "<b>{name}:</b> {y} (#percent%)",
                showInLegend: true,
                name: "May",
                dataPoints: [
                    { y: 400, label: "Water Filter" },
                    { y: 500, label: "Modern Chair" },
                    { y: 220, label: "VOIP Phone" },
                    { y: 350, label: "Microwave" },
                    { y: 220, label: "Water Filter" },
                    { y: 474, label: "Expresso Machine" },
                    { y: 450, label: "Lobby Chair" }
                ]
            },
            {
                type: "stackedBar100",
                toolTipContent: "<b>{name}:</b> {y} (#percent%)",
                showInLegend: true,
                name: "June",
                dataPoints: [
                    { y: 300, label: "Water Filter" },
                    { y: 610, label: "Modern Chair" },
                    { y: 215, label: "VOIP Phone" },
                    { y: 221, label: "Microwave" },
                    { y: 75, label: "Water Filter" },
                    { y: 310, label: "Expresso Machine" },
                    { y: 340, label: "Lobby Chair" }
                ]
        }]
    });
    
    chart1.render();
    chart2.render();
    
    }
    