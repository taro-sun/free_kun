from django.shortcuts import render, get_object_or_404, redirect, render_to_response
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from blog.models import Salse,SalseMans
from blog.forms import SalsesForm, hizuke1Form
import datetime
import json
from django.db.models import Q
from django.contrib.auth.context_processors import auth
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required
from django.db import transaction


@login_required
def post_list(request):

    if hasattr(request, 'user'):
        user = request.user
    else:
        from django.contrib.auth.models import AnonymousUser
        user = AnonymousUser()

    to_date = datetime.date.today()
    from_date = datetime.date(to_date.year, to_date.month, 1)
    id = [i.name for i in SalseMans.objects.raw('SELECT * FROM blog_SalseMans')]
    name = [i.man_id for i in SalseMans.objects.raw('SELECT * FROM blog_SalseMans')]
    
    List = []
    for i in zip(name, id):
        ID   = i[0]
        name = i[1]
        data = Salse.objects.filter(Q(sales_date__range=[from_date, to_date]),Q(sale_man=ID))
        data = sum([i.sales_mount for i in data])
        y    = {"y":data,"label":name}            
        List.append(y) 
    
    # sales = Salse.objects.all().order_by('id')
    sales = Salse.objects.filter(sales_date__range=[from_date, to_date]).order_by('sales_date').reverse()
    form1 = hizuke1Form()

    return render(request,
                'blog/summary.html',     # 使用するテンプレート
                {'sales': sales,'form1':form1,'data':List,'test':List,'from_date':from_date,'to_date':to_date})         # テンプレートに渡すデータ

@login_required
def kensaku(request):
    
    if request.method == 'POST':
        from_date = request.POST['hizuke1']
        to_date   = request.POST['hizuke2']
        id        = [i.name for i in SalseMans.objects.raw('SELECT * FROM blog_SalseMans')]
        name      = [i.man_id for i in SalseMans.objects.raw('SELECT * FROM blog_SalseMans')]
            
        List = []
        for i in zip(name, id):
            ID   = i[0]
            name = i[1]
            data = Salse.objects.filter(Q(sales_date__range=[from_date, to_date]),Q(sale_man=ID))
            data = sum([i.sales_mount for i in data])  
            y    = {"y":data,"label":name}    
            List.append(y) 
    
        sales = Salse.objects.filter(sales_date__range=[from_date, to_date]).order_by('sales_date').reverse()
        form1 = hizuke1Form()


        from_date = datetime.datetime.strptime(from_date, '%Y-%m-%d')
        to_date   = datetime.datetime.strptime(to_date, '%Y-%m-%d')

        return render(request,
                'blog/summary.html',     # 使用するテンプレート
                {'sales': sales,'form1':form1,'data':List, 'from_date':from_date, 'to_date':to_date})
        
    else:

        to_date   = datetime.date.today()
        from_date = datetime.date(to_date.year, to_date.month, 1)

        id   = [i.name for i in SalseMans.objects.raw('SELECT * FROM blog_SalseMans')]
        name = [i.man_id for i in SalseMans.objects.raw('SELECT * FROM blog_SalseMans')]
        
        List = []
        for i in zip(name, id):
            ID   = i[0]
            name = i[1]

            data = Salse.objects.filter(Q(sales_date__range=[from_date, to_date]),Q(sale_man=ID))
            data = sum([i.sales_mount for i in data])    
            y = {"y":data,"label":name}                
            List.append(y) 

        sales = Salse.objects.filter(sales_date__range=[from_date, to_date]).order_by('sales_date').reverse()
        form1 = hizuke1Form()
        

        return render(request,
                    'blog/summary.html',     # 使用するテンプレート
                    {'sales': sales,'form1':form1,'data':List, 'from_date':from_date, 'to_date':to_date})    # テンプレートに渡すデータ




# @permission_required('blog.can_delete', login_url='/login/')
def edit(request,sales_id=None):

    if sales_id:   # sales_id が指定されている (修正時)
        if not request.user.has_perm('blog.can_delete'):
            return redirect('blog:summary')
        else:
            salse = get_object_or_404(Salse, pk=sales_id)
    else:         # sales_id が指定されていない (追加時)
        salse = Salse()

    if request.method == 'POST':
        form = SalsesForm(request.POST, instance=salse)  # POST された request データからフォームを作成
        if form.is_valid():    # フォームのバリデーション
            salse = form.save(commit=False)
            try:
                salse.save()
                return redirect('blog:summary')
            except IntegrityError:
                transaction.rollback()
    else:    # GET の時
        form = SalsesForm(instance=salse)  # salse インスタンスからフォームを作成

    return render(request, 'blog/edit.html', dict(form=form, sales_id=sales_id))



@login_required
def sales_del(request, sales_id):
    """売り上げの削除"""
    sales = get_object_or_404(Salse, pk=sales_id)
    sales.delete()
    return redirect('blog:summary')



@login_required
def redilect(request):
    return render(request, 'blog/summary.html')