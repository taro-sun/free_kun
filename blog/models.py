from django.db import models
from django.utils import timezone
# Create your models here.
class SalseMans(models.Model):
    "担当者"
    man_id = models.IntegerField(primary_key=True)
    name   = models.CharField(max_length=20, unique=True)
    # man_id = models.IntegerField()
    # name   = models.CharField(max_length=20)
    
    def __str__(self):
        return str(self.man_id) + ':' + str(self.name)

class Salse(models.Model):
    sale_man     = models.ForeignKey(SalseMans, on_delete=models.CASCADE,verbose_name="担当者")
    sales_date   = models.DateField(default=timezone.now,verbose_name="売上日")
    sales_mount  = models.IntegerField(verbose_name="金額")
    created_date = models.DateTimeField(default=timezone.now)





