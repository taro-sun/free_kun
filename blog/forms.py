from django.forms import ModelForm
from blog.models import Salse,SalseMans
from django import forms
import datetime


class SalsesForm(ModelForm):
    "売り上げのフォーム" 
    class Meta:
        model = Salse
        fields = ('sale_man','sales_date','sales_mount',)
        

def Today():
    return datetime.date.today()

def Beginning_of_month():
    Today = datetime.date.today()
    return datetime.date(Today.year, Today.month, 1)
    
class hizuke1Form(forms.Form):
    hizuke1 = forms.DateField(initial=Beginning_of_month, widget=forms.DateInput(attrs={'class':'form-control'}))
    hizuke2 = forms.DateField(initial=Today, widget=forms.DateInput(attrs={'class':'form-control'}))


    