from django.conf.urls import include, url
from . import views
from django.contrib.auth import views as auth_views
# from django.contrib.auth.views import login,logout

app_name = "blog"

urlpatterns = [
    url(r'^$', views.post_list, name='post_list'),
    url(r'^kensaku$', views.kensaku, name='kensaku'),
    url(r'^summury$', views.post_list, name='summary'),
    url(r'^add$', views.edit, name='add'),  # 登録
    url(r'^edit/(?P<sales_id>\d+)/$', views.edit, name='edit'),  # 修正
    url(r'^del/(?P<sales_id>\d+)/$', views.sales_del, name='sales_del'),   # 削除
    url(r'^redilect/$', views.post_list, name='redilect'),
    url(
        r'^login/$',
        auth_views.login,
        {'template_name': 'blog/login.html'},
        name='login'
    ),
    url(
        r'^logout/$',
        auth_views.logout,
        {'template_name': 'blog/login.html'},
        name='logout'
    ),
    
]

